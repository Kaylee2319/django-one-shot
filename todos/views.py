from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from .forms import AddTodoListform, AddTodoItemform

# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todo_temps/lists.html", context)


def todo_list_detail(request, id):
    tditems = get_object_or_404(TodoList, id=id)
    context = {
        "todo_items": tditems,
    }
    return render(request, "todo_temps/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = AddTodoListform(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = AddTodoListform()
        context = {
            "form": form,
        }
    return render(request, "todo_temps/create.html", context)


def todo_list_update(request, id):
    post = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = AddTodoListform(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = AddTodoListform(instance=post)
        context = {
            "todo_object": post,
            "form": form,
        }
        return render(request, "todo_temps/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todo_temps/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = AddTodoItemform(request.POST)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = AddTodoItemform()
    context = {
        "form": form,
    }
    return render(request, "todo_temps/add_items.html", context)


def todo_item_update(request, id):
    post = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = AddTodoItemform(request.POST, instance=post)
        if form.is_valid():
            item = form.save()
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = AddTodoItemform(instance=post)
        context = {
            "todo_object": post,
            "form": form,
        }
        return render(request, "todo_temps/edit_items.html", context)
